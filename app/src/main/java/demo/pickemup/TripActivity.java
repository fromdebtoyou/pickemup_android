package demo.pickemup;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import demo.pickemup.adapter.TripViewHolder;
import demo.pickemup.helper.Cache;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;
import demo.pickemup.vo.UserRequestsVO;

public class TripActivity extends BaseAppActivity {

    private String userid;
    String responseData="";
    ListView passengerList;
    ArrayList<UserRequestsVO> passengers=new ArrayList<>();
    String myPassengers="";

    String tripId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_trip);
        configureLayout();

        Intent objIntent = getIntent();
        responseData = objIntent.getStringExtra("responseData");
        passengerList=(ListView) findViewById(R.id.passengerList);
        myPassengers=sharedPref.getString("myPassengers","");

        Button endTripBtn=(Button)findViewById(R.id.endTrip);
        userid = sharedPref.getString("userid", "0");
        tripId=sharedPref.getString("saveTripId","0").replace("\n","").trim();
        drawPassengerList();

        endTripBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CompleteTripTask().execute(Url.COMPLETETRIP);
            }
        });
    }

    private void drawPassengerList(){
        try{

            JSONArray resJsonArray =new JSONArray(responseData);
            for(int count=0;count<resJsonArray.length();count++){
                JSONObject tripResObj=(JSONObject)resJsonArray.get(count);
                if(tripResObj.getJSONObject("trip").getString("id").equalsIgnoreCase(tripId)){
                    JSONArray tripReqDetailsArr=tripResObj.getJSONArray("tripReqDetails");
                    for(int i=0;i<tripReqDetailsArr.length();i++){
                        JSONObject tripReqDetailsObj=(JSONObject) tripReqDetailsArr.get(i);
                        UserRequestsVO userRequestsVO=new UserRequestsVO();
                        userRequestsVO.setOrigin(Cache.getLocationMapCache(sharedPref).get(tripReqDetailsObj.getJSONObject("tripRequest").getInt("startPointId")));
                        userRequestsVO.setDestination(Cache.getLocationMapCache(sharedPref).get(tripReqDetailsObj.getJSONObject("tripRequest").getInt("endPointId")));
                        userRequestsVO.setTimestart(tripReqDetailsObj.getJSONObject("tripRequest").getString("journeyTime"));
                        userRequestsVO.setRqeuestid(tripReqDetailsObj.getJSONObject("tripRequest").getString("id"));
                        userRequestsVO.setUsername(tripReqDetailsObj.getJSONObject("requestedBy").getString("name"));
                        userRequestsVO.setJourneystatus(tripReqDetailsObj.getString("singleTripStatus"));
                        userRequestsVO.setUserid(tripReqDetailsObj.getJSONObject("requestedBy").getString("id"));
                        userRequestsVO.setImageurl(tripReqDetailsObj.getJSONObject("requestedBy").getString("imageURL"));
                        userRequestsVO.setTripid(Integer.parseInt(tripId));
                        passengers.add(userRequestsVO);
                    }
                }
            }
            TripViewHolder tripViewHolder=new TripViewHolder(passengers,TripActivity.this);
            passengerList.setAdapter(tripViewHolder);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class CompleteTripTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            BufferedReader reader = null;
            try {
                URL url = new URL(urlString);
                JSONObject json=new JSONObject();
                json.put("tripId",tripId);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000 /* milliseconds */);
                urlConnection.setConnectTimeout(15000 /* milliseconds */);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(json.toString());
                writer.flush();
                writer.close();
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                responseData = sb.toString();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseData;
        }
        protected void onPostExecute(String result) {
            if(responseData.contains("errCode")){
                AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(TripActivity.this);
                alertDialogBuilder.setTitle("Failed");
                alertDialogBuilder.setMessage("One or more single journeys are on going, cannot end trip!");
                alertDialogBuilder.setPositiveButton(
                        "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            else{
                editor.remove("saveTripFlag");
                editor.remove("saveTripId");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }
        }
    }
}
