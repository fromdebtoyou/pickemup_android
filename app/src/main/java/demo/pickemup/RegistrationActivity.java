package demo.pickemup;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import demo.pickemup.helper.ImageFilePath;
import demo.pickemup.helper.MultipartUtility;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;

public class RegistrationActivity extends Activity {
    private Spinner make;
    private Spinner model;
    private Spinner color;
    private Spinner homeLoc;
    private Spinner officeLoc;
    private Spinner isProvider;
    private Button buttonBrowse;
    private Button buttonReg;
    private EditText email;
    private EditText name;
    private EditText mobile;
    private EditText password;
    private EditText regNo;
    private Spinner homeDepartureTimeHr;
    private Spinner homeDepartureTimeMin;
    private Spinner ofcDepartureTimeHr;
    private Spinner ofcDepartureTimeMin;
    private CheckBox monday;
    private CheckBox tuesday;
    private CheckBox wednesday;
    private CheckBox thursday;
    private CheckBox friday;
    private CheckBox saturday;
    private CheckBox sunday;
    private TextView fileName;
    private LinearLayout regNoLayout;
    private LinearLayout makeLayout;
    private LinearLayout modelLayout;
    private LinearLayout colorLayout;

    ProgressDialog pd;

    ArrayList<String> makeSpinnerList = new ArrayList<>();
    HashMap<Integer,String> makeSpinnerMap = new HashMap<>();
    ArrayList<String> modelSpinnerList = new ArrayList<>();
    HashMap<Integer,String> modelSpinnerMap = new HashMap<>();
    ArrayList<String> colorSpinnerList = new ArrayList<>();
    HashMap<Integer,String> colorSpinnerMap = new HashMap<>();
    ArrayList<String> homeLocSpinnerList = new ArrayList<>();
    HashMap<Integer,String> homeLocSpinnerMap = new HashMap<>();
    ArrayList<String> officeLocSpinnerList = new ArrayList<>();
    HashMap<Integer,String> officeLocSpinnerMap = new HashMap<>();

    String makeId;
    String makeName;
    String modelId;
    String modelName;
    String colorId;
    String colorName;
    String homeLocId;
    String homeLocName;
    String officeLocId;
    String officeLocName;

    String fileLocation;

    final private int PICK_IMAGE_REQUEST  = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        make=(Spinner)findViewById(R.id.make);
        model=(Spinner)findViewById(R.id.model);
        color=(Spinner)findViewById(R.id.color);
        homeLoc=(Spinner)findViewById(R.id.homeLoc);
        officeLoc=(Spinner)findViewById(R.id.ofcLoc);
        buttonBrowse=(Button)findViewById(R.id.buttonBrowse);
        buttonReg=(Button)findViewById(R.id.buttonReg);
        email=(EditText)findViewById(R.id.email);
        isProvider=(Spinner)findViewById(R.id.isProvider);
        name=(EditText)findViewById(R.id.name);
        mobile=(EditText)findViewById(R.id.mobile);
        password=(EditText)findViewById(R.id.password);
        regNo=(EditText)findViewById(R.id.regNo);
        homeDepartureTimeHr=(Spinner)findViewById(R.id.homeDepartureTimeHr);
        homeDepartureTimeMin=(Spinner)findViewById(R.id.homeDepartureTimeMin);
        ofcDepartureTimeHr=(Spinner)findViewById(R.id.ofcDepartureTimeHr);
        ofcDepartureTimeMin=(Spinner)findViewById(R.id.ofcDepartureTimeMin);
        monday=(CheckBox)findViewById(R.id.monday);
        tuesday=(CheckBox)findViewById(R.id.tuesday);
        wednesday=(CheckBox)findViewById(R.id.wednesday);
        thursday=(CheckBox)findViewById(R.id.thursday);
        friday=(CheckBox)findViewById(R.id.friday);
        saturday=(CheckBox)findViewById(R.id.saturday);
        sunday=(CheckBox)findViewById(R.id.sunday);
        fileName=(TextView)findViewById(R.id.fileName);
        regNoLayout=(LinearLayout)findViewById(R.id.regNoLayout);
        makeLayout=(LinearLayout)findViewById(R.id.makeLayout);
        modelLayout=(LinearLayout)findViewById(R.id.modelLayout);
        colorLayout=(LinearLayout)findViewById(R.id.colorLayout);

        new PopulateMakeTask().execute(Url.MAKE);
        new PopulateHomeLocTask().execute(Url.SHOWPOINTS);

        make.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                makeName=makeSpinnerList.get(position);
                makeId=makeSpinnerMap.get(position);
                new PopulateModelTask().execute(Url.MODEL+"?makeId="+makeId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        model.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                modelName=modelSpinnerList.get(position);
                modelId=modelSpinnerMap.get(position);
                new PopulateColorTask().execute(Url.COLOR+"?modelId="+modelId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        color.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                colorName=colorSpinnerList.get(position);
                colorId=colorSpinnerMap.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        homeLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                homeLocName=homeLocSpinnerList.get(position);
                homeLocId=homeLocSpinnerMap.get(position);
                System.out.print(Url.WHERETOGO+"?routepoint="+homeLocId);
                new PopulateOfficeLocTask().execute(Url.WHERETOGO+"?routepoint="+homeLocId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        officeLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                officeLocName=officeLocSpinnerList.get(position);
                officeLocId=officeLocSpinnerMap.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        isProvider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println(position);
                if(position==1){
                    makeLayout.setVisibility(View.GONE);
                    modelLayout.setVisibility(View.GONE);
                    colorLayout.setVisibility(View.GONE);
                    regNoLayout.setVisibility(View.GONE);
                    makeId="";
                    modelId="";
                    colorId="";
                }else{
                    makeLayout.setVisibility(View.VISIBLE);
                    modelLayout.setVisibility(View.VISIBLE);
                    colorLayout.setVisibility(View.VISIBLE);
                    regNoLayout.setVisibility(View.VISIBLE);
                    regNo.setText("");
                    new PopulateMakeTask().execute(Url.MAKE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, 1);
            }
        });

        buttonReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg=getFormValdationMsg();
                if(msg.equalsIgnoreCase("")){
                    new RegistrationTask().execute(Url.REGISTER);
                }else{
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(RegistrationActivity.this);
                    alertDialogBuilder.setTitle("error in data entry");
                    alertDialogBuilder.setMessage(msg);
                    alertDialogBuilder.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            fileLocation = ImageFilePath.getPath(RegistrationActivity.this, data.getData());
            fileName.setText(fileLocation.substring(fileLocation.lastIndexOf('/') + 1));
        }
    }

    private String getFormValdationMsg(){
        String msg="";
        if("".equalsIgnoreCase(fileLocation)){
            msg="Please select profile image !";
        }
        else if(email.getText().toString().equalsIgnoreCase("")){
            msg="Please provide email !";
        }
        else if(name.getText().toString().equalsIgnoreCase("")){
            msg="Please provide name !";
        }
        else if(mobile.getText().toString().equalsIgnoreCase("")){
            msg="Please provide mobile no !";
        }
        else if(mobile.getText().toString().length()!=10){
            msg="Mobile no should be 10 digits !";
        }
        else if(password.getText().toString().equalsIgnoreCase("")){
            msg="Please provide password !";
        }
        else if(password.getText().toString().length()<4){
            msg="Password should be minimum 4 character !";
        }
        else if(isProvider.getSelectedItem().toString().equalsIgnoreCase("yes")){
            if(regNo.getText().toString().equalsIgnoreCase("")){
                msg="Please provide registration number !";
            }
        }
        else{
            File file = new File(fileLocation);
            long fileSizeInBytes = file.length();
            long fileSizeInKB = fileSizeInBytes / 1024;
            if(fileSizeInKB>1024){
                msg="File size should not be more than 1 MB !";
            }
        }
        return msg;
    }

    private class PopulateMakeTask extends AsyncTask<String, String, String> {
        private String responseData;

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try{
                JSONArray jsonArray = new JSONArray(responseData);
                makeSpinnerList.clear();
                makeSpinnerMap.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    makeSpinnerList.add(jsonObject.getString("name"));
                    makeSpinnerMap.put(i,jsonObject.getString("id"));
                }
                makeId=makeSpinnerMap.get(0).toString();
                makeName=makeSpinnerList.get(0).toString();
            }catch (Exception e){
                e.printStackTrace();
            }
            ArrayAdapter<String> adapter=new ArrayAdapter<String>(RegistrationActivity.this,android.R.layout.simple_spinner_dropdown_item,makeSpinnerList);
            make.setAdapter(adapter);
        }
    }

    private class PopulateModelTask extends AsyncTask<String, String, String> {
        private String responseData;

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            System.out.print(responseData);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try{
                System.out.print(responseData);
                JSONArray jsonArray = new JSONArray(responseData);
                modelSpinnerList.clear();
                modelSpinnerMap.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    modelSpinnerList.add(jsonObject.getString("name"));
                    modelSpinnerMap.put(i,jsonObject.getString("id"));
                }
                modelId=modelSpinnerMap.get(0).toString();
                modelName=modelSpinnerList.get(0).toString();
            }catch (Exception e){
                e.printStackTrace();
            }
            ArrayAdapter<String> adapter=new ArrayAdapter<String>(RegistrationActivity.this,android.R.layout.simple_spinner_dropdown_item,modelSpinnerList);
            model.setAdapter(adapter);
        }
    }

    private class PopulateColorTask extends AsyncTask<String, String, String> {
        private String responseData;

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try{
                JSONArray jsonArray = new JSONArray(responseData);
                colorSpinnerList.clear();
                colorSpinnerMap.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    colorSpinnerList.add(jsonObject.getString("name"));
                    colorSpinnerMap.put(i,jsonObject.getString("id"));
                }
                colorId=colorSpinnerMap.get(0).toString();
                colorName=colorSpinnerList.get(0).toString();
            }catch (Exception e){
                e.printStackTrace();
            }
            ArrayAdapter<String> adapter=new ArrayAdapter<String>(RegistrationActivity.this,android.R.layout.simple_spinner_dropdown_item,colorSpinnerList);
            color.setAdapter(adapter);
        }
    }

    private class PopulateHomeLocTask extends AsyncTask<String, String, String> {
        private String responseData;

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try{
                JSONArray jsonArray = new JSONArray(responseData);
                homeLocSpinnerList.clear();
                homeLocSpinnerMap.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    homeLocSpinnerList.add(jsonObject.getString("description"));
                    homeLocSpinnerMap.put(i,jsonObject.getString("id"));
                }
                homeLocId=homeLocSpinnerMap.get(0).toString();
                homeLocName=homeLocSpinnerList.get(0).toString();
            }catch (Exception e){
                e.printStackTrace();
            }
            ArrayAdapter<String> adapter=new ArrayAdapter<String>(RegistrationActivity.this,android.R.layout.simple_spinner_dropdown_item,homeLocSpinnerList);
            homeLoc.setAdapter(adapter);
        }
    }

    private class PopulateOfficeLocTask extends AsyncTask<String, String, String> {
        private String responseData;

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try{
                System.out.println(responseData);
                JSONArray jsonArray = new JSONArray(responseData);
                officeLocSpinnerList.clear();
                officeLocSpinnerMap.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    officeLocSpinnerList.add(jsonObject.getString("description"));
                    officeLocSpinnerMap.put(i,jsonObject.getString("id"));
                }
                officeLocId=officeLocSpinnerMap.get(0).toString();
                officeLocName=officeLocSpinnerList.get(0).toString();
            }catch (Exception e){
                e.printStackTrace();
            }
            ArrayAdapter<String> adapter=new ArrayAdapter<String>(RegistrationActivity.this,android.R.layout.simple_spinner_dropdown_item,officeLocSpinnerList);
            officeLoc.setAdapter(adapter);
        }
    }

    private class RegistrationTask extends AsyncTask<String, String, String> {
        private String responseData;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("RegistrationTask Start");
            pd = new ProgressDialog(RegistrationActivity.this);
            pd.setTitle("Please Wait..");
            pd.setMessage("Loading…");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String charset = "UTF-8";
            File uploadFile = new File(fileLocation);

            String requestURL = params[0];

            try {
                String isDriver;
                if(isProvider.getSelectedItem().toString().equalsIgnoreCase("Yes")){
                    isDriver="true";
                }else{
                    isDriver="false";
                }
                String officeActiveDays="[";
                if(monday.isChecked()){
                    officeActiveDays+="true,";
                }else{
                    officeActiveDays+="false,";
                }
                if(tuesday.isChecked()){
                    officeActiveDays+="true,";
                }else{
                    officeActiveDays+="false,";
                }
                if(wednesday.isChecked()){
                    officeActiveDays+="true,";
                }else{
                    officeActiveDays+="false,";
                }
                if(thursday.isChecked()){
                    officeActiveDays+="true,";
                }else{
                    officeActiveDays+="false,";
                }
                if(friday.isChecked()){
                    officeActiveDays+="true,";
                }else{
                    officeActiveDays+="false,";
                }
                if(saturday.isChecked()){
                    officeActiveDays+="true,";
                }else{
                    officeActiveDays+="false,";
                }
                if(sunday.isChecked()){
                    officeActiveDays+="true";
                }else{
                    officeActiveDays+="false";
                }
                officeActiveDays+="]";

                MultipartUtility multipart = new MultipartUtility(requestURL, charset);
                multipart.addFormField("mobileNumber", mobile.getText().toString());
                multipart.addFormField("emailId", email.getText().toString());
                multipart.addFormField("password", password.getText().toString());
                multipart.addFormField("name", name.getText().toString());
                multipart.addFormField("homeLocation", homeLocId);
                multipart.addFormField("homeDepartTime", homeDepartureTimeHr.getSelectedItem().toString()+":"+homeDepartureTimeMin.getSelectedItem().toString()+":00");
                multipart.addFormField("officeLocation", officeLocId);
                multipart.addFormField("officeDepartTime", ofcDepartureTimeHr.getSelectedItem().toString()+":"+ofcDepartureTimeMin.getSelectedItem().toString()+":00");
                multipart.addFormField("isProvider", isDriver);
                multipart.addFormField("vehicleRegdNum", regNo.getText().toString());
                multipart.addFormField("vehicleMake", makeId);
                multipart.addFormField("vehicleModel", modelId);
                multipart.addFormField("vehicleColor", colorId);
                multipart.addJsonFormField("officeActiveDays", officeActiveDays);

                multipart.addFilePart("profileImage", uploadFile);

                List<String> response = multipart.finish();

                System.out.println("SERVER REPLIED:");

                for (String line : response) {
                    responseData+=line;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return responseData;
        }

        protected void onPostExecute(String result) {
            try {
                pd.dismiss();
                System.out.println(responseData);
                if (responseData != null && !responseData.contains("errCode")) {
                    Toast.makeText(getApplicationContext(), "User successfully created !!",
                            Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Request Failed",
                            Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}