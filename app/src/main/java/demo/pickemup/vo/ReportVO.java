package demo.pickemup.vo;

import java.util.List;

/**
 * Created by ARINDAM on 08-07-2018.
 */

public class ReportVO {
    private int routeId;
    private String routeTxt;
    private int originId;
    private String originTxt;
    private int destinationId;
    private String destinationTxt;
    private String journeyDate;
    private String journeyTime;
    private int requestId;
    private int tripId;
    private List<Integer> requestIds;
    private String driverName;
    private String driverMobile;
    private String makeName;
    private String modelName;
    private String regNo;

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String getRouteTxt() {
        return routeTxt;
    }

    public void setRouteTxt(String routeTxt) {
        this.routeTxt = routeTxt;
    }

    public int getOriginId() {
        return originId;
    }

    public void setOriginId(int originId) {
        this.originId = originId;
    }

    public String getOriginTxt() {
        return originTxt;
    }

    public void setOriginTxt(String originTxt) {
        this.originTxt = originTxt;
    }

    public int getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(int destinationId) {
        this.destinationId = destinationId;
    }

    public String getDestinationTxt() {
        return destinationTxt;
    }

    public void setDestinationTxt(String destinationTxt) {
        this.destinationTxt = destinationTxt;
    }

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }

    public String getJourneyTime() {
        return journeyTime;
    }

    public void setJourneyTime(String journeyTime) {
        this.journeyTime = journeyTime;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public List<Integer> getRequestIds() {
        return requestIds;
    }

    public void setRequestIds(List<Integer> requestIds) {
        this.requestIds = requestIds;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getMakeName() {
        return makeName;
    }

    public void setMakeName(String makeName) {
        this.makeName = makeName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }
}
