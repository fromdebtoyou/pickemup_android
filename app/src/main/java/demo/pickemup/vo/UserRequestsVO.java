package demo.pickemup.vo;

/**
 * Created by earmghh on 1/12/2018.
 */

public class UserRequestsVO {
    private String userid;
    private String username;
    private String imageurl;
    private String startpointid;
    private String origin;
    private String endpointid;
    private String destination;
    private String rqeuestid;
    private String timestart;
    private boolean selected;
    private int otp;
    private String journeystatus;
    private int otpid;
    private boolean validated;
    private int tripid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getStartpointid() {
        return startpointid;
    }

    public void setStartpointid(String startpointid) {
        this.startpointid = startpointid;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getEndpointid() {
        return endpointid;
    }

    public void setEndpointid(String endpointid) {
        this.endpointid = endpointid;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getRqeuestid() {
        return rqeuestid;
    }

    public void setRqeuestid(String rqeuestid) {
        this.rqeuestid = rqeuestid;
    }

    public String getTimestart() {
        return timestart;
    }

    public void setTimestart(String timestart) {
        this.timestart = timestart;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    public String getJourneystatus() {
        return journeystatus;
    }

    public void setJourneystatus(String journeystatus) {
        this.journeystatus = journeystatus;
    }

    public int getOtpid() {
        return otpid;
    }

    public void setOtpid(int otpid) {
        this.otpid = otpid;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public int getTripid() {
        return tripid;
    }

    public void setTripid(int tripid) {
        this.tripid = tripid;
    }
}
