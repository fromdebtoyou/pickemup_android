package demo.pickemup.vo;

/**
 * Created by DELL on 17-09-2017.
 */
public class PickUpPoints {

    private int pickuppointid;
    private int routeid;
    private String pickupPoint;
    private int isActive;
    private int isOrigin;
    private int isDestination;

    public PickUpPoints() {

    }
    public PickUpPoints(int pickuppointid, int routeid, String pickupPoint,
                          int isActive) {
        super();
        this.pickuppointid = pickuppointid;
        this.routeid = routeid;
        this.pickupPoint = pickupPoint;
        this.isActive = isActive;
    }


    public int getIsOrigin() {
        return isOrigin;
    }
    public void setIsOrigin(int isOrigin) {
        this.isOrigin = isOrigin;
    }
    public int getIsDestination() {
        return isDestination;
    }
    public void setIsDestination(int isDestination) {
        this.isDestination = isDestination;
    }
    public int getPickuppointid() {
        return pickuppointid;
    }
    public void setPickuppointid(int pickuppointid) {
        this.pickuppointid = pickuppointid;
    }
    public int getRouteid() {
        return routeid;
    }
    public void setRouteid(int routeid) {
        this.routeid = routeid;
    }
    public String getPickupPoint() {
        return pickupPoint;
    }
    public void setPickupPoint(String pickupPoint) {
        this.pickupPoint = pickupPoint;
    }
    public int getIsActive() {
        return isActive;
    }
    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }
}
