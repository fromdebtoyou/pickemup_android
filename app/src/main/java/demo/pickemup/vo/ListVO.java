package demo.pickemup.vo;

/**
 * Created by earmghh on 12/31/2017.
 */

public class ListVO {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String id;
    private String description;

    public ListVO(String id, String description) {
        this.id = id;
        this.description = description;
    }
}
