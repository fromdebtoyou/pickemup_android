package demo.pickemup.vo;

import java.io.Serializable;
import java.util.ArrayList;

import demo.pickemup.adapter.DataModel;

/**
 * Created by DELL on 17-09-2017.
 */
public class RouteVO implements Serializable{

    private int routeid;
    private String origin;
    private String desttination;
    private String time;
    private String capacity;
    private String fare;
    private ArrayList<DataModel> pickPoints;
    private String providername;
    private String providerid;
    private String userid;

    public RouteVO() {
    }

    public RouteVO(String origin, String desttination, String time, String capacity, String fare, ArrayList<DataModel> pickPoints) {
        this.origin = origin;
        this.desttination = desttination;
        this.time = time;
        this.capacity = capacity;
        this.fare = fare;
        this.pickPoints = pickPoints;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getProvidername() {
        return providername;
    }

    public void setProvidername(String providername) {
        this.providername = providername;
    }

    public String getProviderid() {
        return providerid;
    }

    public void setProviderid(String providerid) {
        this.providerid = providerid;
    }

    public int getRouteid() {
        return routeid;
    }

    public void setRouteid(int routeid) {
        this.routeid = routeid;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDesttination() {
        return desttination;
    }

    public void setDesttination(String desttination) {
        this.desttination = desttination;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    public ArrayList<DataModel> getPickPoints() {
        return pickPoints;
    }

    public void setPickPoints(ArrayList<DataModel> pickPoints) {
        this.pickPoints = pickPoints;
    }
}
