package demo.pickemup.vo;

import java.util.HashMap;

/**
 * Created by DELL on 16-09-2017.
 */
public class UserVO {

    public int userid;
    public String username;
    public String mobilenumber;
    public String password;
    public String deviceid;
    public String loginstatus;
    HashMap<String,Boolean> myPassengers=new HashMap<>();

    public String getLoginstatus() {
        return loginstatus;
    }
    public void setLoginstatus(String loginstatus) {
        this.loginstatus = loginstatus;
    }
    public int getUserid() {
        return userid;
    }
    public void setUserid(int userid) {
        this.userid = userid;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getMobilenumber() {
        return mobilenumber;
    }
    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getDeviceid() {
        return deviceid;
    }
    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }


    public HashMap<String, Boolean> getMyPassengers() {
        return myPassengers;
    }

    public void setMyPassengers(HashMap<String, Boolean> myPassengers) {
        this.myPassengers = myPassengers;
    }
}
