package demo.pickemup.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import org.json.JSONArray;
import org.json.JSONObject;

import demo.pickemup.LoginActivity;
import demo.pickemup.R;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;

/**
 * Created by ARINDAM on 24-03-2018.
 */

public class EndTripBackgroundService extends Service {

    private boolean isRunning;
    private Context context;
    private Thread backgroundThread;
    private String userid;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        this.context = this;
        this.isRunning = false;
        this.backgroundThread = new Thread(myTask);

    }

    private Runnable myTask = new Runnable() {
        public void run() {
            new NotificationTask().execute(Url.NOTIFICATION+"?userid="+userid+"&type=Completion%20of%20Journey");
            stopSelf();
        }
    };

    @Override
    public void onDestroy() {
        this.isRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.userid=intent.getExtras().getString("userid");
        if(!this.isRunning) {
            this.isRunning = true;
            this.backgroundThread.start();
        }
        return START_STICKY;
    }


    private class NotificationTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try{
                JSONArray jsonArray = new JSONArray(responseData);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    System.out.println(object.getString("otp"));
                    NotificationCompat.Builder builder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.preloader)
                                    .setContentTitle("Pickemup Notification")
                                    .setContentText("OTP for completion of your trip is "+object.getString("otp"));
                    Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    builder.setSound(alarmSound);

                    Intent notificationIntent = new Intent(context, LoginActivity.class);
                    PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(contentIntent);

                    // Add as notification
                    NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.notify(0, builder.build());
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
