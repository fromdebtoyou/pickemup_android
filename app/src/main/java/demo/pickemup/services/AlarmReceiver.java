package demo.pickemup.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by ARINDAM on 24-03-2018.
 */

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent background = new Intent(context, BackgroundService.class);
        background.putExtra("userid",intent.getExtras().getString("userid"));
        context.startService(background);
    }

}