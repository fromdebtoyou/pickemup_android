package demo.pickemup;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import demo.pickemup.adapter.DensityViewHolder;
import demo.pickemup.adapter.ProviderModel;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;

public class ProviderActivity extends BaseAppActivity {

    private AutoCompleteTextView origin;
    private Spinner destination;
    private CheckBox routeChkBox;
    private TextView selectedRoute;
    private Button findRequest;
    private Spinner starttimehr;
    private Spinner starttimemin;
    private String userid;

    HashMap<String,String> orginMaster=new HashMap<>();
    String routeList[]=null;
    String routeIdList[]=null;
    HashMap<String,String> routeMaster=new HashMap<>();
    HashMap<String,String> routeMasterByName=new HashMap<>();
    ArrayList<String> destinationIdList=new ArrayList<>();
    ArrayList<String> destinationDescList=new ArrayList<>();
    String originId="";
    String destinationId="";
    String routeId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_provider);
        configureLayout();

        origin=(AutoCompleteTextView)findViewById(R.id.origin);
        destination=(Spinner) findViewById(R.id.destination);
        routeChkBox=(CheckBox) findViewById(R.id.routeChkBox);
        selectedRoute=(TextView) findViewById(R.id.selectedRoute);
        findRequest= (Button) findViewById(R.id.findRequest);
        starttimehr=(Spinner) findViewById(R.id.starttimehr);
        starttimemin=(Spinner) findViewById(R.id.starttimemin);

        userid = sharedPref.getString("userid", "0");

        new PopulateOriginTask().execute(Url.SHOWPOINTS);

        origin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                originId=orginMaster.get(parent.getAdapter().getItem(position).toString());
                new PopulateDestinationTask().execute(Url.WHERETOGO+"?routepoint="+originId);
            }
        });

        destination.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                destinationId=destinationIdList.get(position);
                routeChkBox.setChecked(false);
                selectedRoute.setText("You have not selected any route !!");
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        routeChkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    if(originId.equalsIgnoreCase("")){
                        routeChkBox.setChecked(false);
                        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(ProviderActivity.this);
                        alertDialogBuilder.setMessage("Please select origin!");
                        alertDialogBuilder.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        final AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }else{
                        new PopulateRouteTagsTask().execute(Url.GETROUTETAGS+"?start="+originId+"&end="+destinationId);
                    }
                }else{
                    selectedRoute.setText("You have not selected any route !!");
                }
            }
        });

        findRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!sharedPref.getString("saveTripFlag","T").equals("T")){
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(ProviderActivity.this);
                    alertDialogBuilder.setTitle("Error in data entry");
                    alertDialogBuilder.setMessage("You already submitted a trip !");
                    alertDialogBuilder.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                else if(originId.equalsIgnoreCase("")||destinationId.equalsIgnoreCase("")||routeChkBox.isChecked()==false){
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(ProviderActivity.this);
                    alertDialogBuilder.setTitle("Error in data entry");
                    alertDialogBuilder.setMessage("Please select origin, destination & route to get user request!");
                    alertDialogBuilder.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                else{
                    Intent intent = new Intent(getApplicationContext(), RequestListActivity.class);
                    intent.putExtra("routeid",routeId);
                    intent.putExtra("starttimehr",starttimehr.getSelectedItem().toString());
                    intent.putExtra("starttimemin",starttimemin.getSelectedItem().toString());
                    intent.putExtra("startpoint",originId);
                    intent.putExtra("endpoint",destinationId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                    ProviderActivity.this.startActivity(intent);
                    ProviderActivity.this.finish();
                }
            }
        });
    }

    private class PopulateOriginTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }
        protected void onPostExecute(String result) {
            String[] origins=null;
            try{
                JSONArray jsonArray = new JSONArray(responseData);
                origins=new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject originObj = jsonArray.getJSONObject(i);
                    origins[i]=originObj.getString("description");
                    orginMaster.put(originObj.getString("description"),originObj.getString("id"));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            ArrayAdapter<String> adapter=new ArrayAdapter<String>(ProviderActivity.this,android.R.layout.simple_spinner_dropdown_item,origins);
            origin.setAdapter(adapter);
            origin.setThreshold(1);
        }
    }

    private class PopulateDestinationTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try{
                JSONArray jsonArray = new JSONArray(responseData);
                destinationIdList.clear();
                destinationDescList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject originObj = jsonArray.getJSONObject(i);
                    destinationIdList.add(originObj.getString("id"));
                    destinationDescList.add(originObj.getString("description"));
                }
                destinationId=destinationIdList.get(0);
            }catch (Exception e){
                e.printStackTrace();
            }
            ArrayAdapter<String> adapter=new ArrayAdapter<String>(ProviderActivity.this,android.R.layout.simple_spinner_dropdown_item,destinationDescList);
            destination.setAdapter(adapter);
        }
    }

    private class PopulateRouteTagsTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {

            try{
                JSONArray jsonArray = new JSONArray(responseData);
                routeList=new String[jsonArray.length()];
                routeIdList=new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    routeList[i]=object.getString("description");
                    routeIdList[i]=object.getString("id");
                    routeMaster.put(object.getString("id"),object.getString("description"));
                    routeMasterByName.put(object.getString("description"),object.getString("id"));
                }
                new FindRouteDensityTAsk().execute(Url.ROUTEDENSITY);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private class FindRouteDensityTAsk extends AsyncTask<String, String, String> {
        private String responseData;

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            InputStream in = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(urlString);
                JSONArray jsonArray=new JSONArray();
                for(int cnt=0;cnt<routeIdList.length;cnt++){
                    JSONObject jsonObject=new JSONObject();
                    jsonObject.put("key",routeIdList[cnt]);
                    jsonArray.put(jsonObject);
                }
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(jsonArray.toString());
                writer.flush();
                writer.close();
                // Get the server response
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                responseData = sb.toString();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseData;
        }

        protected void onPostExecute(String result) {
            try {
                System.out.println(responseData);
                JSONObject jsonObj = new JSONObject(responseData);
                Iterator keysIterator = jsonObj.keys();
                final ArrayList<ProviderModel> arrayList=new ArrayList<>();
                while(keysIterator.hasNext()) {
                    String key = (String) keysIterator.next();
                    System.out.println(jsonObj.getJSONArray(key).toString());
                    String description=routeMaster.get(key);
                    JSONArray densityArr=jsonObj.getJSONArray(key);
                    for(int count=0;count<densityArr.length();count++){
                        JSONObject densityObj=(JSONObject) densityArr.get(count);
                        ProviderModel providerModel=new ProviderModel(description,densityObj.getString("starttime"),densityObj.getString("endtime"),densityObj.getString("count"));
                        arrayList.add(providerModel);
                    }
                }
                if(arrayList.size()>0){
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(ProviderActivity.this);
                    alertDialogBuilder.setTitle("Route Density");
                    alertDialogBuilder
                            .setPositiveButton("Cancel",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    routeChkBox.toggle();
                                    dialog.dismiss();
                                }
                            });
                    LayoutInflater inflater = getLayoutInflater();
                    View convertView = (View) inflater.inflate(R.layout.custom_list_view, null);
                    alertDialogBuilder.setView(convertView);
                    ListView customList = (ListView) convertView.findViewById(R.id.customList);
                    DensityViewHolder densityViewHolder=new DensityViewHolder(arrayList,ProviderActivity.this);
                    customList.setAdapter(densityViewHolder);
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                    customList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            ProviderModel providerModel=arrayList.get(position);

                            routeId=routeMasterByName.get(providerModel.getRoutename());
                            System.out.println("Time>>>>>>>>>>>>>"+providerModel.getStarttime()+">>"+routeId);

                            //starttimehr=providerModel.getStarttime().split(":")[0];
                            selectedRoute.setText("Selected route is "+providerModel.getRoutename());
                            alertDialog.dismiss();
                        }
                    });
                }else{
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(ProviderActivity.this);
                    alertDialogBuilder.setMessage("No request found!");
                    alertDialogBuilder.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    routeChkBox.setChecked(false);
                                    selectedRoute.setText("You have not selected any route !!");
                                    dialog.cancel();
                                }
                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
