package demo.pickemup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import demo.pickemup.adapter.MyTripViewHolder;
import demo.pickemup.adapter.NotificationViewHolder;
import demo.pickemup.helper.Cache;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;
import demo.pickemup.vo.ReportVO;

public class MyTripActivity extends BaseAppActivity{
    String userid="";
    ListView myTripList;
    TextView nodatamsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_trip);
        configureLayout();

        userid = sharedPref.getString("userid","0");

        myTripList=(ListView)findViewById(R.id.myTripList);
        nodatamsg=(TextView)findViewById(R.id.nodatamsg);
        new FetchMyTripListTask().execute(Url.GETNOTIFICATIONS+"?user-id="+userid+"&driver-ack=N");
    }

    private class FetchMyTripListTask extends AsyncTask<String, String, String> {
        private String responseData;
        private final ProgressDialog dialog = new ProgressDialog(MyTripActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }
        protected void onPostExecute(String result) {
            try {
                System.out.println(responseData);
                ArrayList<ReportVO> reportVOS=new ArrayList<>();
                JSONArray jsonArray=new JSONArray(responseData);
                if(jsonArray.length()==0){
                    myTripList.setVisibility(View.GONE);
                    nodatamsg.setVisibility(View.VISIBLE);
                }else{
                    nodatamsg.setVisibility(View.GONE);
                    myTripList.setVisibility(View.VISIBLE);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=(JSONObject)jsonArray.get(i);
                        ReportVO reportVO=new ReportVO();
                        reportVO.setRequestId(jsonObject.getJSONObject("tripRequest").getInt("id"));
                        reportVO.setOriginId(jsonObject.getJSONObject("tripRequest").getInt("startPointId"));
                        reportVO.setOriginTxt(Cache.getLocationMapCache(sharedPref).get(reportVO.getOriginId()));
                        reportVO.setDestinationId(jsonObject.getJSONObject("tripRequest").getInt("endPointId"));
                        reportVO.setDestinationTxt(Cache.getLocationMapCache(sharedPref).get(reportVO.getDestinationId()));
                        SimpleDateFormat format1 = new SimpleDateFormat("ddMMyyyy");
                        SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
                        Date date = format1.parse(jsonObject.getJSONObject("tripRequest").getString("journeyDate").toString());
                        reportVO.setJourneyDate(format2.format(date));
                        reportVO.setJourneyTime(jsonObject.getJSONObject("tripRequest").getString("journeyTime").toString());
                        reportVO.setRouteId(jsonObject.getJSONObject("tripRequest").getInt("routeId"));
                        reportVOS.add(reportVO);
                    }
                    MyTripViewHolder myTripViewHolder = new MyTripViewHolder(reportVOS, getApplicationContext());
                    myTripList.setAdapter(myTripViewHolder);
                }
                dialog.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }




}
