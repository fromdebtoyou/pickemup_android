package demo.pickemup;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import demo.pickemup.adapter.MyOngoingTripViewHolder;
import demo.pickemup.adapter.MyTripViewHolder;
import demo.pickemup.helper.Cache;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;
import demo.pickemup.vo.ReportVO;

public class MyOngoingTripActivity extends BaseAppActivity{
    String userid="";
    ListView myOngoingTripList;
    TextView nodatamsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ongoing_trip);
        configureLayout();

        userid = sharedPref.getString("userid","0");

        myOngoingTripList=(ListView)findViewById(R.id.myOngoingTripList);
        nodatamsg=(TextView)findViewById(R.id.nodatamsg);

        new FetchMyOngoingTripListTask().execute(Url.GETMYONGOINGTRIPS+"?user-id="+userid+"&state=ON_GOING");

    }

    private class FetchMyOngoingTripListTask extends AsyncTask<String, String, String> {
        private String responseData;
        private final ProgressDialog dialog = new ProgressDialog(MyOngoingTripActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }
        protected void onPostExecute(String result) {
            try {
                System.out.println(responseData);
                ArrayList<ReportVO> reportVOS=new ArrayList<>();
                JSONArray jsonArray=new JSONArray(responseData);
                if(jsonArray.length()==0){
                    myOngoingTripList.setVisibility(View.GONE);
                    nodatamsg.setVisibility(View.VISIBLE);
                }else{
                    nodatamsg.setVisibility(View.GONE);
                    myOngoingTripList.setVisibility(View.VISIBLE);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=(JSONObject)jsonArray.get(i);
                        ReportVO reportVO=new ReportVO();
                        reportVO.setRequestId(Integer.parseInt(jsonObject.getJSONObject("trip").getJSONArray("requestPoolId").get(0).toString()));
                        reportVO.setOriginId(jsonObject.getJSONObject("trip").getInt("startPoint"));
                        reportVO.setDestinationId(jsonObject.getJSONObject("trip").getInt("endPoint"));
                        reportVO.setOriginTxt(Cache.getLocationMapCache(sharedPref).get(reportVO.getOriginId()));
                        reportVO.setDestinationTxt(Cache.getLocationMapCache(sharedPref).get(reportVO.getDestinationId()));
                        reportVO.setJourneyDate(jsonObject.getJSONObject("trip").getString("startTime").toString().substring(0,10));
                        reportVO.setJourneyTime(jsonObject.getJSONObject("trip").getString("startTime").toString().substring(11,16));
                        reportVO.setTripId(jsonObject.getJSONObject("trip").getInt("id"));
                        reportVO.setDriverName(jsonObject.getJSONObject("providedBy").getString("name"));
                        reportVO.setDriverMobile(jsonObject.getJSONObject("providedBy").getString("mobileNumber"));
                        reportVO.setMakeName(jsonObject.getJSONObject("providedBy").getJSONObject("vehicleSpec").getJSONObject("make").getString("name"));
                        reportVO.setModelName(jsonObject.getJSONObject("providedBy").getJSONObject("vehicleSpec").getJSONObject("model").getString("name"));
                        reportVO.setRegNo(jsonObject.getJSONObject("providedBy").getString("vehicleRegd"));
                        reportVOS.add(reportVO);
                    }
                    MyOngoingTripViewHolder myOngoingTripViewHolder = new MyOngoingTripViewHolder(reportVOS, getApplicationContext());
                    myOngoingTripList.setAdapter(myOngoingTripViewHolder);
                }
                dialog.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


}
