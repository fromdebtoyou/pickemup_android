package demo.pickemup.helper;

import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ARINDAM on 28-07-2018.
 */

public class Cache {

    public static HashMap<Integer,String> getLocationMapCache(SharedPreferences sp)throws Exception{
        System.out.println("~~~~~~~"+sp.getString("locationMapCache","[]"));
        JSONArray jsonArray = new JSONArray(sp.getString("locationMapCache","[]"));
        HashMap<Integer,String> locationMapCache = new HashMap<>();
        locationMapCache.clear();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            locationMapCache.put(jsonObject.getInt("id"),jsonObject.getString("description"));
        }
        for(Map.Entry m:locationMapCache.entrySet()){
            System.out.print(m.getKey()+"-----"+m.getValue());
        }
        return locationMapCache;
    }
}
