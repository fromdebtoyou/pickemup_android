package demo.pickemup.helper;

/**
 * Created by earmghh on 1/1/2018.
 */

public class Url {
    //static String BASEURL="http://192.168.42.234:8080";

    static String BASEURL="http://18.191.116.95:8080";
    public static final String SHOWPOINTS=BASEURL+"/PickemupServices/api/common/showPoints";
    public static final String WHERETOGO=BASEURL+"/PickemupServices/api/common/whereToGo";
    public static final String GETROUTETAGS=BASEURL+"/PickemupServices/api/common/getRouteTags";
    public static final String SAVEROUTEREQUEST=BASEURL+"/PickemupServices/api/trip/request/new/consumer";
    public static final String LOGIN=BASEURL+"/PickemupServices/api/user/login";
    public static final String ROUTEDENSITY=BASEURL+"/PickemupServices/api/random/getRouteDesnity";
    public static final String USERSFORROUTEANDSLOT=BASEURL+"/PickemupServices/api/trip/route/party/consumer";
    public static final String SAVETRIP=BASEURL+"/PickemupServices/api/trip/create/new/provider";
    public static final String STARTTRIP=BASEURL+"/PickemupServices/api/trip/start/party/provider";
    public static final String STARTSINGLEJOURNEY=BASEURL+"/PickemupServices/api/trip/start/party/consumer";
    public static final String FETCHTRIP=BASEURL+"/PickemupServices/api/trip/party/provider";
    public static final String ENDSINGLEJOURNEY=BASEURL+"/PickemupServices/api/trip/end/party/consumer";
    public static final String COMPLETETRIP=BASEURL+"/PickemupServices/api/trip/end/party/provider";
    public static final String NOTIFICATION=BASEURL+"/PickemupServices/api/intrip/notification";
    public static final String MAKE=BASEURL+"/PickemupServices/api/vehicle/makes";
    public static final String MODEL=BASEURL+"/PickemupServices/api/vehicle/models";
    public static final String COLOR=BASEURL+"/PickemupServices/api/vehicle/colors";
    public static final String REGISTER=BASEURL+"/PickemupServices/api/user/register";
    public static final String GETNOTIFICATIONS=BASEURL+"/PickemupServices/api/trip/requests/party/consumer";
    public static final String ACKNOTIFICATION=BASEURL+"/PickemupServices/api/trip/request/consumer/response";
    public static final String GETMYONGOINGTRIPS=BASEURL+"/PickemupServices/api/trip/party/consumer";



}
