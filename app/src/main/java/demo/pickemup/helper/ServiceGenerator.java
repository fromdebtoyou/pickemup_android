package demo.pickemup.helper;

import android.app.AlarmManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by ARINDAM on 26-05-2018.
 */

public class ServiceGenerator extends AppCompatActivity {
    public AlarmManager getAlarmManager(){
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        return alarmManager;
    }
}
