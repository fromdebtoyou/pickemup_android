package demo.pickemup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import demo.pickemup.R;
import demo.pickemup.vo.RouteVO;

public class RoutesCustomAdapter extends ArrayAdapter<RouteVO> implements View.OnClickListener{

    private ArrayList<RouteVO> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtOrigin;
        TextView txtDestination;
        TextView txtTime;
        ImageView editRoute;
    }

    public RoutesCustomAdapter(ArrayList<RouteVO> data, Context context) {
        super(context, R.layout.routes_row_item, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        RouteVO dataModel=(RouteVO)object;

        dataSet.remove(position);
        this.notifyDataSetChanged();


    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        RouteVO dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.routes_row_item, parent, false);
            viewHolder.txtOrigin = (TextView) convertView.findViewById(R.id.routeorigin);
            viewHolder.txtDestination = (TextView) convertView.findViewById(R.id.routedestination);
            viewHolder.txtTime = (TextView) convertView.findViewById(R.id.routetime);
            viewHolder.editRoute = (ImageView) convertView.findViewById(R.id.routeSelector);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtOrigin.setText(dataModel.getOrigin());
        viewHolder.txtDestination.setText(dataModel.getDesttination());
        viewHolder.txtTime.setText(dataModel.getTime());
        viewHolder.editRoute.setOnClickListener(this);
        viewHolder.editRoute.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }
}
