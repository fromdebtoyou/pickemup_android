package demo.pickemup.adapter;

public class DataModel {

    String name;
    String latitude;
    String longitude;
    String feature;

    public DataModel(String name, String latitude, String longitude, String feature ) {
        this.name=name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.feature=feature;

    }

    public String getName() {
        return name;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getFeature() {
        return feature;
    }

}
