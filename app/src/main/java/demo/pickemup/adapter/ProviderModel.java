package demo.pickemup.adapter;

public class ProviderModel {

    String routename;
    String starttime;
    String endtime;
    String density;

    public ProviderModel(String routename, String starttime, String endtime, String density ) {
        this.routename=routename;
        this.starttime = starttime;
        this.endtime = endtime;
        this.density=density;

    }

    public String getRoutename() {
        return routename;
    }

    public String getStarttime() {
        return starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public String getDensity() {
        return density;
    }
}
