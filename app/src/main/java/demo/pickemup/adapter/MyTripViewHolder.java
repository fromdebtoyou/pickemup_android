package demo.pickemup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import demo.pickemup.R;
import demo.pickemup.vo.ReportVO;

/**
 * Created by earmghh on 1/10/2018.
 */

public class MyTripViewHolder extends ArrayAdapter<ReportVO>{

    private ArrayList<ReportVO> dataSet;
    Context mContext;

    private static class ViewHolder {
        TextView requestIdTxt;
        TextView sourceDestination;
        TextView journeyDate;
        TextView journeyTime;
    }

    public MyTripViewHolder(ArrayList<ReportVO> data, Context context) {
        super(context, R.layout.my_trip_row_item, data);
        this.dataSet = data;
        this.mContext=context;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ReportVO dataModel = getItem(position);
        MyTripViewHolder.ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new MyTripViewHolder.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.my_trip_row_item, parent, false);
            viewHolder.requestIdTxt = (TextView) convertView.findViewById(R.id.requestIdTxt);
            viewHolder.sourceDestination = (TextView) convertView.findViewById(R.id.sourceDestination);
            viewHolder.journeyDate = (TextView) convertView.findViewById(R.id.journeyDate);
            viewHolder.journeyTime = (TextView) convertView.findViewById(R.id.journeyTime);

            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyTripViewHolder.ViewHolder) convertView.getTag();
            result=convertView;
        }
        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.requestIdTxt.setText("Request ID - "+dataModel.getRequestId());
        viewHolder.sourceDestination.setText(dataModel.getOriginTxt()+" to "+dataModel.getDestinationTxt());
        viewHolder.journeyDate.setText("Date - "+dataModel.getJourneyDate());
        viewHolder.journeyTime.setText("Time - "+dataModel.getJourneyTime());
        return convertView;
    }
}

