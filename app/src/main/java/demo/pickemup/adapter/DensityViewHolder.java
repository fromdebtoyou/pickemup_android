package demo.pickemup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import demo.pickemup.R;

/**
 * Created by earmghh on 1/10/2018.
 */

public class DensityViewHolder extends ArrayAdapter<ProviderModel> implements View.OnClickListener{

    private ArrayList<ProviderModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView routename;
        TextView timeslot;
        TextView density;
    }

    public DensityViewHolder(ArrayList<ProviderModel> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;
    }

    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        DataModel dataModel=(DataModel)object;

        dataSet.remove(position);
        this.notifyDataSetChanged();
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ProviderModel dataModel = getItem(position);
        DensityViewHolder.ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new DensityViewHolder.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.provider_row_item, parent, false);
            viewHolder.routename = (TextView) convertView.findViewById(R.id.routename);
            viewHolder.timeslot = (TextView) convertView.findViewById(R.id.timeslot);
            viewHolder.density = (TextView) convertView.findViewById(R.id.density);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (DensityViewHolder.ViewHolder) convertView.getTag();
            result=convertView;
        }
        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.routename.setText("Route - "+dataModel.getRoutename());
        viewHolder.timeslot.setText(dataModel.getStarttime()+" to "+dataModel.getEndtime());
        viewHolder.density.setText(dataModel.getDensity());
        return convertView;
    }
}

