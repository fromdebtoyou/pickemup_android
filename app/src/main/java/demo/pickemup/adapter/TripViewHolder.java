package demo.pickemup.adapter;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import demo.pickemup.R;
import demo.pickemup.TripActivity;
import demo.pickemup.helper.ImageLoader;
import demo.pickemup.helper.ServiceGenerator;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;
import demo.pickemup.services.AlarmReceiver;
import demo.pickemup.services.EndTripAlarmReceiver;
import demo.pickemup.vo.UserRequestsVO;

/**
 * Created by earmghh on 1/10/2018.
 */

public class TripViewHolder extends ArrayAdapter<UserRequestsVO>{

    private ArrayList<UserRequestsVO> dataSet;
    Context mContext;
    SparseBooleanArray mCheckStates;
    Button currentStartBtn;
    UserRequestsVO dataModel;
    public ImageLoader imageLoader;

    // View lookup cache
    private static class ViewHolder {
        TextView name;
        TextView pickuppoint;
        TextView droppoint;
        TextView pickuptime;
        LinearLayout status;
        ImageView image;
        Button start;
        TextView statusTxt;
    }

    public TripViewHolder(ArrayList<UserRequestsVO> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;
        mCheckStates = new SparseBooleanArray(data.size());
        imageLoader=new ImageLoader(context);
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        dataModel = getItem(position);
        final TripViewHolder.ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new TripViewHolder.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.passengers_row_item, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.pickuppoint = (TextView) convertView.findViewById(R.id.pickuppoint);
            viewHolder.droppoint = (TextView) convertView.findViewById(R.id.droppoint);
            viewHolder.pickuptime = (TextView) convertView.findViewById(R.id.pickuptime);
            viewHolder.status = (LinearLayout) convertView.findViewById(R.id.status);
            viewHolder.statusTxt = (TextView) convertView.findViewById(R.id.statusTxt);
            viewHolder.start = (Button) convertView.findViewById(R.id.start);
            viewHolder.image=(ImageView) convertView.findViewById(R.id.image);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (TripViewHolder.ViewHolder) convertView.getTag();
            result=convertView;
        }
        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.name.setText(dataModel.getUsername());
        viewHolder.pickuppoint.setText("Pickup Point - "+dataModel.getOrigin());
        viewHolder.droppoint.setText("Drop Point - "+dataModel.getDestination());
        viewHolder.pickuptime.setText("Pickup Time - "+dataModel.getTimestart());
        if(dataModel.getJourneystatus().equalsIgnoreCase("NOT_STARTED")){
            viewHolder.status.setBackgroundColor(mContext.getResources().getColor(R.color.customRed));
            viewHolder.start.setVisibility(View.VISIBLE);
            viewHolder.statusTxt.setText("Not Started");
        }else if(dataModel.getJourneystatus().equalsIgnoreCase("ON_GOING")){
            viewHolder.status.setBackgroundColor(mContext.getResources().getColor(R.color.customYellow));
            viewHolder.start.setVisibility(View.GONE);
            viewHolder.statusTxt.setText("On Going");
        }else{
            viewHolder.status.setBackgroundColor(mContext.getResources().getColor(R.color.customGreen));
            viewHolder.start.setVisibility(View.GONE);
            viewHolder.statusTxt.setText("Completed");
        }
        viewHolder.start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentStartBtn=viewHolder.start;
                viewHolder.start.setVisibility(View.GONE);
                new StartSingleJourneyTask().execute(Url.STARTSINGLEJOURNEY);
                viewHolder.status.setBackgroundColor(mContext.getResources().getColor(R.color.customYellow));
                viewHolder.statusTxt.setText("On Going");
            }
        });
        imageLoader.DisplayImage(dataModel.getImageurl(),viewHolder.image);
        return convertView;
    }

    private class StartSingleJourneyTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            BufferedReader reader = null;
            try {
                URL url = new URL(urlString);
                JSONObject json=new JSONObject();
                json.put("tripId",dataModel.getTripid());
                json.put("requestId",dataModel.getRqeuestid());
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000 /* milliseconds */);
                urlConnection.setConnectTimeout(15000 /* milliseconds */);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(json.toString());
                writer.flush();
                writer.close();
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                responseData = sb.toString();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseData;
        }
        protected void onPostExecute(String result) {
            System.out.println("responseData>>>>>>>>>>>>>"+responseData);
        }
    }

}

