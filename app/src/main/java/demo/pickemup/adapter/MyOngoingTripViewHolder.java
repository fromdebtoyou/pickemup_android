package demo.pickemup.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import demo.pickemup.DashboardActivity;
import demo.pickemup.MyOngoingTripActivity;
import demo.pickemup.R;
import demo.pickemup.RegistrationActivity;
import demo.pickemup.TripActivity;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;
import demo.pickemup.vo.ReportVO;
import demo.pickemup.vo.UserRequestsVO;

/**
 * Created by earmghh on 1/10/2018.
 */

public class MyOngoingTripViewHolder extends ArrayAdapter<ReportVO>{

    private ArrayList<ReportVO> dataSet;
    Context mContext;
    Context rootContext;
    ReportVO dataModel;

    private static class ViewHolder {
        TextView requestIdTxt;
        TextView sourceDestination;
        TextView journeyDateTime;
        Button completeSingle;
        TextView driverName;
        TextView driverMobile;
        TextView car;
        TextView regNo;
    }

    public MyOngoingTripViewHolder(ArrayList<ReportVO> data, Context context) {
        super(context, R.layout.my_ongoing_trip_row_item, data);
        this.dataSet = data;
        this.mContext=context;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        dataModel = getItem(position);
        MyOngoingTripViewHolder.ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new MyOngoingTripViewHolder.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.my_ongoing_trip_row_item, parent, false);
            viewHolder.requestIdTxt = (TextView) convertView.findViewById(R.id.requestIdTxt);
            viewHolder.sourceDestination = (TextView) convertView.findViewById(R.id.sourceDestination);
            viewHolder.journeyDateTime = (TextView) convertView.findViewById(R.id.journeyDateTime);
            viewHolder.driverName = (TextView) convertView.findViewById(R.id.driverName);
            viewHolder.driverMobile = (TextView) convertView.findViewById(R.id.driverMobile);
            viewHolder.completeSingle=(Button) convertView.findViewById(R.id.completeSingle);
            viewHolder.car=(TextView) convertView.findViewById(R.id.car);
            viewHolder.regNo=(TextView) convertView.findViewById(R.id.regNo);

            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyOngoingTripViewHolder.ViewHolder) convertView.getTag();
            result=convertView;
        }
        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.requestIdTxt.setText("Request ID - "+dataModel.getRequestId());
        viewHolder.sourceDestination.setText(dataModel.getOriginTxt()+" to "+dataModel.getDestinationTxt());
        viewHolder.journeyDateTime.setText("Date and Time - "+dataModel.getJourneyDate()+" "+dataModel.getJourneyTime());
        viewHolder.driverName.setText("Name - "+dataModel.getDriverName());
        viewHolder.driverMobile.setText("Name - "+dataModel.getDriverMobile());
        viewHolder.car.setText(dataModel.getMakeName()+" - "+dataModel.getModelName());
        viewHolder.regNo.setText("Reg No = "+dataModel.getRegNo());

        viewHolder.completeSingle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rootContext=v.getRootView().getContext();
                new CompleteSingleJourneyTask().execute(Url.ENDSINGLEJOURNEY);
            }
        });
        return convertView;
    }

    private class CompleteSingleJourneyTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            BufferedReader reader = null;
            try {
                URL url = new URL(urlString);
                JSONObject json=new JSONObject();
                json.put("tripId",dataModel.getTripId());
                json.put("requestId",dataModel.getRequestId());
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000 /* milliseconds */);
                urlConnection.setConnectTimeout(15000 /* milliseconds */);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(json.toString());
                writer.flush();
                writer.close();
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                responseData = sb.toString();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseData;
        }
        protected void onPostExecute(String result) {
            System.out.println("responseData>>>>>>>>>>>>>"+responseData);
            if(responseData.contains("errCode")){
                AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(rootContext);
                alertDialogBuilder.setTitle("Error");
                alertDialogBuilder.setMessage("Upable to complete your trip.");
                alertDialogBuilder.setPositiveButton(
                        "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }else{
                Intent intent = new Intent(rootContext, MyOngoingTripActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                rootContext.startActivity(intent);
            }
        }
    }
}

