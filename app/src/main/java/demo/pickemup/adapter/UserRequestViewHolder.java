package demo.pickemup.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import demo.pickemup.R;
import demo.pickemup.helper.ImageLoader;
import demo.pickemup.vo.UserRequestsVO;

/**
 * Created by earmghh on 1/10/2018.
 */

public class UserRequestViewHolder extends ArrayAdapter<UserRequestsVO> implements CompoundButton.OnCheckedChangeListener{

    private ArrayList<UserRequestsVO> dataSet;
    Context mContext;
    SparseBooleanArray mCheckStates;
    public ImageLoader imageLoader;

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mCheckStates.put((Integer) buttonView.getTag(), isChecked);
    }

    // View lookup cache
    private static class ViewHolder {
        TextView username;
        TextView origin;
        TextView destination;
        TextView time;
        CheckBox userReqItems;
        ImageView profileImg;
    }

    public UserRequestViewHolder(ArrayList<UserRequestsVO> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;
        mCheckStates = new SparseBooleanArray(data.size());
        imageLoader=new ImageLoader(context);
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UserRequestsVO dataModel = getItem(position);
        UserRequestViewHolder.ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new UserRequestViewHolder.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.user_request_row_item, parent, false);
            viewHolder.username = (TextView) convertView.findViewById(R.id.name);
            viewHolder.origin = (TextView) convertView.findViewById(R.id.origin);
            viewHolder.destination = (TextView) convertView.findViewById(R.id.destination);
            viewHolder.time = (TextView) convertView.findViewById(R.id.time);
            viewHolder.userReqItems= (CheckBox) convertView.findViewById(R.id.userReqItems);
            viewHolder.profileImg=(ImageView) convertView.findViewById(R.id.profileImg);

            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (UserRequestViewHolder.ViewHolder) convertView.getTag();
            result=convertView;
        }
        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.username.setText(dataModel.getUsername());
        viewHolder.origin.setText("Origin - "+dataModel.getOrigin());
        viewHolder.destination.setText("Destination - "+dataModel.getDestination());
        viewHolder.time.setText("Time - "+dataModel.getTimestart());
        viewHolder.userReqItems.setTag(position);
        viewHolder.userReqItems.setChecked(mCheckStates.get(position, false));
        viewHolder.userReqItems.setOnCheckedChangeListener(this);
        imageLoader.DisplayImage(dataModel.getImageurl(), viewHolder.profileImg);
        return convertView;
    }

    public boolean isChecked(int position) {
        return mCheckStates.get(position, false);
    }

    public void setChecked(int position, boolean isChecked) {
        mCheckStates.put(position, isChecked);
    }
    public void toggle(int position) {
        setChecked(position, !isChecked(position));
    }
}

