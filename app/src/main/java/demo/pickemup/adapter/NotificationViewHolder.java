package demo.pickemup.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import demo.pickemup.DashboardActivity;
import demo.pickemup.R;
import demo.pickemup.TripActivity;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;
import demo.pickemup.vo.ReportVO;
import demo.pickemup.vo.UserRequestsVO;

/**
 * Created by earmghh on 1/10/2018.
 */

public class NotificationViewHolder extends ArrayAdapter<ReportVO>{

    private ArrayList<ReportVO> dataSet;
    Context mContext;
    Context rootContext;

    private static class ViewHolder {
        TextView requestIdTxt;
        TextView sourceDestination;
        TextView journeyDateTime;
        TextView notificationDesc;
        TextView driverMobile;
        Button accept;
        Button reject;
    }

    public NotificationViewHolder(ArrayList<ReportVO> data, Context context) {
        super(context, R.layout.notification_row_item, data);
        this.dataSet = data;
        this.mContext=context;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ReportVO dataModel = getItem(position);
        final NotificationViewHolder.ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new NotificationViewHolder.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.notification_row_item, parent, false);
            viewHolder.requestIdTxt = (TextView) convertView.findViewById(R.id.requestIdTxt);
            viewHolder.sourceDestination = (TextView) convertView.findViewById(R.id.sourceDestination);
            viewHolder.journeyDateTime = (TextView) convertView.findViewById(R.id.journeyDateTime);
            viewHolder.notificationDesc = (TextView) convertView.findViewById(R.id.notificationDesc);
            viewHolder.driverMobile = (TextView) convertView.findViewById(R.id.driverMobile);
            viewHolder.accept= (Button) convertView.findViewById(R.id.accept);
            viewHolder.reject= (Button) convertView.findViewById(R.id.reject);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (NotificationViewHolder.ViewHolder) convertView.getTag();
            result=convertView;
        }
        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.requestIdTxt.setText("Request ID - "+dataModel.getRequestId());
        viewHolder.sourceDestination.setText(dataModel.getOriginTxt()+" to "+dataModel.getDestinationTxt());
        viewHolder.journeyDateTime.setText("Date and Time - "+dataModel.getJourneyDate()+" "+dataModel.getJourneyTime());
        viewHolder.notificationDesc.setText(dataModel.getDriverName()+" has added you for the given request.");
        viewHolder.driverMobile.setText("Mobile No - "+dataModel.getDriverMobile());
        viewHolder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.print("position-----"+ dataModel.getRequestId());
                System.out.print("Clicked on accept button");
                rootContext=v.getRootView().getContext();
                new NotificationsAckTask().execute(Url.ACKNOTIFICATION+"@@"+dataModel.getRequestId()+"@@Y");
            }
        });

        viewHolder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.print("position-----"+ dataModel.getRequestId());
                System.out.print("Clicked on reject button");
                rootContext=v.getRootView().getContext();
                new NotificationsAckTask().execute(Url.ACKNOTIFICATION+"@@"+dataModel.getRequestId()+"@@N");
            }
        });
        return convertView;
    }

    private class NotificationsAckTask extends AsyncTask<String, String, String> {
        private String responseData;
        private final ProgressDialog dialog = new ProgressDialog(rootContext);

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String str = params[0];
            String[] strArr=str.split("@@");
            InputStream in = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(strArr[0]);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("tripRequestId", strArr[1]);
                if(strArr[2].equalsIgnoreCase("Y")){
                    postDataParams.put("accepted", true);
                }else{
                    postDataParams.put("accepted", false);
                }
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(postDataParams.toString());
                writer.flush();
                writer.close();
                // Get the server response
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                responseData = sb.toString();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseData;
        }

        protected void onPostExecute(String result) {
            try {
                System.out.println(responseData);
                dialog.dismiss();
                Intent intent = new Intent(rootContext, DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                rootContext.startActivity(intent);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}

