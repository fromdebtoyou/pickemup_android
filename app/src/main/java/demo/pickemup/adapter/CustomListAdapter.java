package demo.pickemup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import demo.pickemup.R;
import demo.pickemup.vo.ListVO;

/**
 * Created by earmghh on 12/31/2017.
 */

public class CustomListAdapter extends ArrayAdapter<ListVO> implements View.OnClickListener{
    private ListVO[] dataSet;
    Context mContext;
    int layoutResourceId;

    private static class ViewHolder {
        TextView txtName;
        TextView txtType;
        TextView txtVersion;
        ImageView info;
    }

    public CustomListAdapter(ListVO[] data,int layoutResourceId, Context context) {
        super(context, layoutResourceId, data);
        this.dataSet = data;
        this.mContext=context;
    }
    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);


        //dataSet.remove(position);
        this.notifyDataSetChanged();

        /*switch (v.getId())
        {
            case R.id.item_info:
                Snackbar.make(v, "Release date " + dataModel.getFeature(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;
        }*/
    }
}
