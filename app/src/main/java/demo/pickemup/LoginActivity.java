package demo.pickemup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;
import demo.pickemup.vo.UserVO;

public class LoginActivity extends AppCompatActivity {
    private EditText usermobile;
    private EditText password;
    private Button buttonLogin;
    private Button buttonRegistration;
    private CheckBox isSaved;

    Context context;
    SharedPreferences.Editor editor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        buttonLogin = (Button)findViewById(R.id.buttonLogin);
        usermobile = (EditText) findViewById(R.id.userid);
        password = (EditText) findViewById(R.id.password);
        buttonRegistration=(Button)findViewById(R.id.buttonRegistration);
        isSaved=(CheckBox)findViewById(R.id.isSaved);
        context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                "pickemup.SharedPref", Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        isReadStoragePermissionGranted();

        if(sharedPref.getBoolean("loggedin",false)){
            new LoginTask().execute(Url.LOGIN, sharedPref.getString("mobileNumber",""), sharedPref.getString("password",""));
        }

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoginTask().execute(Url.LOGIN, usermobile.getText().toString(), password.getText().toString());
            }
        });

        buttonRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    public  boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        }
        else {
            return true;
        }
    }

    private class LoginTask extends AsyncTask<String, String, String> {

        private String responseData;
        private final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage("Processing...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            InputStream in = null;
            UserVO objUser = new UserVO();
            objUser.setUsername(params[1]);
            objUser.setPassword(params[2]);
            BufferedReader reader = null;
            try {
                URL url = new URL(urlString);

                JSONObject postDataParams = new JSONObject();
                postDataParams.put("mobileNumber", objUser.getUsername());
                postDataParams.put("password", objUser.getPassword());

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000 /* milliseconds */);
                urlConnection.setConnectTimeout(15000 /* milliseconds */);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(postDataParams.toString());
                writer.flush();
                writer.close();
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                responseData = sb.toString();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }catch (Exception e) {
                e.printStackTrace();
            }
            return responseData;
        }

        protected void onPostExecute(String result) {
            try {
                JSONObject userObj = new JSONObject(responseData);
                SharedPreferences sharedPref = context.getSharedPreferences(
                        "pickemup.SharedPref", Context.MODE_PRIVATE);
                editor = sharedPref.edit();
                if (responseData != null && !responseData.contains("errCode")) {
                    if(isSaved.isChecked()){
                        editor.putBoolean("loggedin",true);
                        editor.putString("password",password.getText().toString());
                    }
                    new PopulateLocTask().execute(Url.SHOWPOINTS);
                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    editor.putString("userid", userObj.getString("id"));
                    editor.putString("username", userObj.getString("name"));
                    editor.putString("mobileNumber", userObj.getString("mobileNumber"));
                    editor.putString("email", userObj.getString("email"));
                    editor.putBoolean("isProvider", userObj.getBoolean("isProvider"));
                    editor.putInt("officeLocation", userObj.getInt("officeLocation"));
                    editor.putInt("homeLocation", userObj.getInt("homeLocation"));
                    editor.putString("officeDepartTime", userObj.getString("officeDepartTime"));
                    editor.putString("homeDepartTime", userObj.getString("homeDepartTime"));
                    editor.putString("imageURL", userObj.getString("imageURL"));
                    if(userObj.getBoolean("isProvider")){
                        editor.putString("vehicleRegd", userObj.getString("vehicleRegd"));
                        JSONObject vehicleSpecObj=userObj.getJSONObject("vehicleSpec");
                        editor.putString("makeid", vehicleSpecObj.getJSONObject("make").getString("id"));
                        editor.putString("modelid", vehicleSpecObj.getJSONObject("model").getString("id"));
                        editor.putString("colorid", vehicleSpecObj.getJSONObject("color").getString("id"));
                    }
                    if(sharedPref.getBoolean("loggedin",false)){
                        editor.apply();
                    }else{
                        editor.commit();
                    }
                    getApplicationContext().startActivity(intent);
                } else {
                    usermobile.setText("");
                    password.setText("");
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(LoginActivity.this);
                    alertDialogBuilder.setTitle("Failed to login");
                    alertDialogBuilder.setMessage("Wrong mobile no or password !");
                    alertDialogBuilder.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                dialog.dismiss();
            } catch (JSONException e) {
                dialog.dismiss();
                e.printStackTrace();
            }
        }
    }

    public class PopulateLocTask extends AsyncTask<String, String, String> {
        private String responseData;

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try{
                editor.putString("locationMapCache",responseData);
                editor.commit();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}