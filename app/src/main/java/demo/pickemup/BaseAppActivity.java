package demo.pickemup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import demo.pickemup.helper.ImageLoader;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;

/**
 * Created by ARINDAM on 07-07-2018.
 */
public abstract class BaseAppActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mToggle;
    Toolbar mToolbar;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    NavigationView navigationView;
    ImageView profilePic;
    public ImageLoader imageLoader;

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_logout:
                {
                    Context context = getApplicationContext();
                    sharedPref=context.getSharedPreferences(
                            "pickemup.SharedPref", Context.MODE_PRIVATE);
                    editor = sharedPref.edit();
                    editor.remove("loggedin");
                    editor.remove("password");
                    editor.remove("userid");
                    editor.remove("username");
                    editor.remove("mobileNumber");
                    editor.remove("email");
                    editor.remove("isProvider");
                    editor.remove("officeLocation");
                    editor.remove("homeLocation");
                    editor.remove("officeDepartTime");
                    editor.remove("homeDepartTime");
                    editor.remove("imageURL");
                    editor.remove("vehicleRegd");
                    editor.remove("makeid");
                    editor.remove("modelid");
                    editor.remove("colorid");
                    editor.remove("locationMapCache");

                    editor.commit();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                    this.startActivity(intent);
                    this.finish();
                }
                break;
            case R.id.nav_notifications:
                {
                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                    this.startActivity(intent);
                    this.finish();
                }
                break;
            case R.id.nav_search_route:
                {
                    Intent mainIntent = new Intent(this, SearchRouteActivity.class);
                    this.startActivity(mainIntent);
                    this.finish();
                }
                break;
            case R.id.nav_find_passenger:
                {
                    Intent mainIntent = new Intent(this, ProviderActivity.class);
                    this.startActivity(mainIntent);
                    this.finish();
                }
                break;
            case R.id.nav_pending_trip:
                {
                    Intent mainIntent = new Intent(this, MyTripActivity.class);
                    this.startActivity(mainIntent);
                    this.finish();
                }
                break;
            case R.id.nav_ongoing_trip:
                {
                    Intent mainIntent = new Intent(this, MyOngoingTripActivity.class);
                    this.startActivity(mainIntent);
                    this.finish();
                }
                break;
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setNavigationViewListner() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void configureLayout(){
        mToolbar = (Toolbar) findViewById(R.id.nav_action);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setNavigationViewListner();

        Context context = getApplicationContext();
        sharedPref=context.getSharedPreferences(
                "pickemup.SharedPref", Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        View header=navigationView.getHeaderView(0);
        final TextView profileName=(TextView) header.findViewById(R.id.profileName);
        profileName.setText(sharedPref.getString("username",""));
        profilePic=(ImageView) header.findViewById(R.id.profilePic);
        if(!sharedPref.getBoolean("isProvider",false)){
            NavigationView  navigationView = (NavigationView) findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_find_passenger).setVisible(false);
        }
        imageLoader=new ImageLoader(getApplicationContext());
        String URL = sharedPref.getString("imageURL","");
        imageLoader.DisplayImage(URL, profilePic);

    }
}
