package demo.pickemup;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import demo.pickemup.adapter.NotificationViewHolder;
import demo.pickemup.helper.Cache;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;
import demo.pickemup.vo.ReportVO;

public class DashboardActivity extends BaseAppActivity{
    String userid="";
    Button startTripBtn;
    Button fetchTrip;
    ListView notificationList;
    TextView nodatamsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        configureLayout();

        userid = sharedPref.getString("userid","0");
        startTripBtn=(Button)findViewById(R.id.startTrip);
        notificationList=(ListView)findViewById(R.id.notificationList);
        nodatamsg=(TextView)findViewById(R.id.nodatamsg);
        fetchTrip=(Button)findViewById(R.id.fetchTrip);

        startTripBtn.setVisibility(View.GONE);
        fetchTrip.setVisibility(View.GONE);
        String saveTripFlag=sharedPref.getString("saveTripFlag","N");

        new NotificationsTask().execute(Url.GETNOTIFICATIONS+"?user-id="+userid+"&driver-ack=true");

        if("Y".equalsIgnoreCase(saveTripFlag)){
            startTripBtn.setVisibility(View.VISIBLE);
            fetchTrip.setVisibility(View.GONE);
        }

        if("S".equalsIgnoreCase(saveTripFlag)){
            startTripBtn.setVisibility(View.GONE);
            fetchTrip.setVisibility(View.VISIBLE);
        }

        startTripBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new StartTripTask().execute(Url.STARTTRIP);
                startTripBtn.setVisibility(View.GONE);
                fetchTrip.setVisibility(View.VISIBLE);
                SharedPreferences.Editor editor=sharedPref.edit();
                editor.putString("saveTripFlag","S");
                editor.apply();
                editor.commit();
            }
        });

        fetchTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FetchTripTask().execute(Url.FETCHTRIP+"?user-id="+userid+"&state=ON_GOING");
            }
        });

    }

    private class StartTripTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            BufferedReader reader = null;
            try {
                URL url = new URL(urlString);
                JSONObject json=new JSONObject();
                json.put("tripId",sharedPref.getString("saveTripId","0"));
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000 /* milliseconds */);
                urlConnection.setConnectTimeout(15000 /* milliseconds */);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(json.toString());
                writer.flush();
                writer.close();
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                responseData = sb.toString();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseData;
        }

        protected void onPostExecute(String result) {
            AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(DashboardActivity.this);
            alertDialogBuilder.setMessage("Your trip has been started. Fetch details to manage your trip. ");
            alertDialogBuilder.setPositiveButton(
                    "Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }


    private class FetchTripTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }
        protected void onPostExecute(String result) {
            Intent intent = new Intent(getApplicationContext(), TripActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("responseData",responseData);
            getApplicationContext().startActivity(intent);
        }
    }

    private class NotificationsTask extends AsyncTask<String, String, String> {
        private String responseData;
        private final ProgressDialog dialog = new ProgressDialog(DashboardActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try {
                ArrayList<ReportVO> reportVOS=new ArrayList<>();
                JSONArray jsonArray=new JSONArray(responseData);
                if(jsonArray.length()==0){
                    notificationList.setVisibility(View.GONE);
                    nodatamsg.setVisibility(View.VISIBLE);
                }else{
                    nodatamsg.setVisibility(View.GONE);
                    notificationList.setVisibility(View.VISIBLE);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=(JSONObject)jsonArray.get(i);
                        ReportVO reportVO=new ReportVO();
                        reportVO.setRequestId(jsonObject.getJSONObject("tripRequest").getInt("id"));
                        reportVO.setOriginId(jsonObject.getJSONObject("tripRequest").getInt("startPointId"));
                        reportVO.setOriginTxt(Cache.getLocationMapCache(sharedPref).get(reportVO.getOriginId()));
                        reportVO.setDestinationId(jsonObject.getJSONObject("tripRequest").getInt("endPointId"));
                        reportVO.setDestinationTxt(Cache.getLocationMapCache(sharedPref).get(reportVO.getDestinationId()));
                        SimpleDateFormat format1 = new SimpleDateFormat("ddMMyyyy");
                        SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
                        Date date = format1.parse(jsonObject.getJSONObject("tripRequest").getString("journeyDate").toString());
                        reportVO.setJourneyDate(format2.format(date));
                        reportVO.setJourneyTime(jsonObject.getJSONObject("tripRequest").getString("journeyTime").toString());
                        reportVO.setRouteId(jsonObject.getJSONObject("tripRequest").getInt("routeId"));
                        reportVO.setDriverName(jsonObject.getJSONObject("providedBy").getString("name"));
                        reportVO.setDriverMobile(jsonObject.getJSONObject("providedBy").getString("mobileNumber"));
                        reportVOS.add(reportVO);
                    }
                    NotificationViewHolder notificationViewHolder = new NotificationViewHolder(reportVOS, getApplicationContext());
                    notificationList.setAdapter(notificationViewHolder);
                }
                dialog.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


}
