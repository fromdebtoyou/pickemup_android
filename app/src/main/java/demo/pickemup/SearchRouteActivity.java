package demo.pickemup;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import demo.pickemup.adapter.CustomAdapter;
import demo.pickemup.adapter.DataModel;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;
import demo.pickemup.services.AlarmReceiver;
import demo.pickemup.services.EndTripAlarmReceiver;
import demo.pickemup.vo.PickUpPoints;
import demo.pickemup.vo.RouteVO;

public class SearchRouteActivity extends BaseAppActivity {

    private AutoCompleteTextView origin;
    private Spinner destination;
    private CheckBox routeChkBox;
    private TextView selectedRoute;
    private Button saveRequest;
    private Spinner starttimehr;
    private Spinner starttimemin;
    private String userid;

    HashMap<String,String> orginMaster=new HashMap<>();
    String routeList[]=null;
    HashMap<String,String> routeMaster=new HashMap<>();
    ArrayList<String> destinationIdList=new ArrayList<>();
    ArrayList<String> destinationDescList=new ArrayList<>();
    String originId="";
    String destinationId="";
    String routeId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_search_route);
        configureLayout();

        origin=(AutoCompleteTextView)findViewById(R.id.origin);
        destination=(Spinner) findViewById(R.id.destination);
        routeChkBox=(CheckBox) findViewById(R.id.routeChkBox);
        selectedRoute=(TextView) findViewById(R.id.selectedRoute);
        saveRequest= (Button) findViewById(R.id.saveRequest);
        starttimehr=(Spinner) findViewById(R.id.starttimehr);
        starttimemin=(Spinner) findViewById(R.id.starttimemin);

        userid = sharedPref.getString("userid", "0");

        new PopulateOriginTask().execute(Url.SHOWPOINTS);

        origin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                originId=orginMaster.get(parent.getAdapter().getItem(position).toString());
                new PopulateDestinationTask().execute(Url.WHERETOGO+"?routepoint="+originId);
            }
        });

        destination.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                destinationId=destinationIdList.get(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        routeChkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    if(originId.equalsIgnoreCase("")){
                        routeChkBox.setChecked(false);
                        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(SearchRouteActivity.this);
                        alertDialogBuilder.setMessage("Please select origin!");
                        alertDialogBuilder.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        final AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }else{
                        new PopulateRouteTagsTask().execute(Url.GETROUTETAGS+"?start="+originId+"&end="+destinationId);
                    }
                }else{
                    selectedRoute.setText("You have not selected any route !!");
                }
            }
        });

        saveRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(originId.equalsIgnoreCase("")||destinationId.equalsIgnoreCase("")||routeChkBox.isChecked()==false){
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(SearchRouteActivity.this);
                    alertDialogBuilder.setTitle("Error in data entry");
                    alertDialogBuilder.setMessage("Please select origin, destination & route to save your request !");
                    alertDialogBuilder.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }else{
                    new SaveRouteRequestTask().execute(Url.SAVEROUTEREQUEST);
                }
            }
        });

    }

    private class PopulateOriginTask extends AsyncTask<String, String, String> {
        private String responseData;

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            String[] origins=null;
            try{
                JSONArray jsonArray = new JSONArray(responseData);
                origins=new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject originObj = jsonArray.getJSONObject(i);
                    origins[i]=originObj.getString("description");
                    orginMaster.put(originObj.getString("description"),originObj.getString("id"));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            ArrayAdapter<String> adapter=new ArrayAdapter<String>(SearchRouteActivity.this,android.R.layout.simple_spinner_dropdown_item,origins);
            origin.setAdapter(adapter);
            origin.setThreshold(1);
        }
    }

    private class PopulateDestinationTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try{
                JSONArray jsonArray = new JSONArray(responseData);
                destinationIdList.clear();
                destinationDescList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject originObj = jsonArray.getJSONObject(i);
                    destinationIdList.add(originObj.getString("id"));
                    destinationDescList.add(originObj.getString("description"));
                }
                destinationId=destinationIdList.get(0);
            }catch (Exception e){
                e.printStackTrace();
            }
            ArrayAdapter<String> adapter=new ArrayAdapter<String>(SearchRouteActivity.this,android.R.layout.simple_spinner_dropdown_item,destinationDescList);
            destination.setAdapter(adapter);
        }
    }

    private class PopulateRouteTagsTask extends AsyncTask<String, String, String> {
        private String responseData;
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            try{
                JSONArray jsonArray = new JSONArray(responseData);
                routeList=new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    routeList[i]=object.getString("description");
                    routeMaster.put(object.getString("description"),object.getString("id"));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(SearchRouteActivity.this);
            alertDialogBuilder.setTitle("Routes");
            alertDialogBuilder
                    .setPositiveButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        routeChkBox.toggle();
                        dialog.dismiss();
                    }
                    });
            LayoutInflater inflater = getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.custom_list_view, null);
            alertDialogBuilder.setView(convertView);
            ListView lv = (ListView) convertView.findViewById(R.id.customList);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(SearchRouteActivity.this,android.R.layout.simple_list_item_single_choice,routeList);
            lv.setAdapter(adapter);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                routeId=routeMaster.get(routeList[position]);
                selectedRoute.setText("Selected route is "+routeList[position]);
                alertDialog.dismiss();
                }
            });
        }
    }

    private class SaveRouteRequestTask extends AsyncTask<String, String, String> {
        private String responseData;

        private final ProgressDialog dialog = new ProgressDialog(SearchRouteActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage("Processing...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            InputStream in = null;
            BufferedReader reader = null;
            try {
                Date date = new Date();
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.setTime(date);
                String journeyDate="";
                int currentHour=calendar.get(Calendar.HOUR_OF_DAY);
                int selectedHour=Integer.parseInt(starttimehr.getSelectedItem().toString());
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                if(selectedHour<currentHour){
                    System.out.println(dateFormat.format(date));
                    Calendar c = Calendar.getInstance();
                    c.setTime(date);
                    c.add(Calendar.DATE, 1);
                    date = c.getTime();
                    journeyDate=dateFormat.format(date);
                }else{
                    journeyDate=dateFormat.format(date);
                }
                URL url = new URL(urlString);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("userId", userid);
                postDataParams.put("startPointId", originId);
                postDataParams.put("endPointId", destinationId);
                postDataParams.put("journeyDate", journeyDate);
                postDataParams.put("routeId", routeId);
                postDataParams.put("journeyTime", starttimehr.getSelectedItem().toString()+":"+starttimemin.getSelectedItem().toString());
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(postDataParams.toString());
                writer.flush();
                writer.close();
                // Get the server response
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                responseData = sb.toString();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseData;
        }

        protected void onPostExecute(String result) {
            try {
                dialog.dismiss();
                if (responseData != null && !responseData.contains("errCode")) {
                    Toast.makeText(getApplicationContext(), "Your request saved successfully",
                            Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                } else {
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(SearchRouteActivity.this);
                    alertDialogBuilder.setTitle("Failed");
                    alertDialogBuilder.setMessage("We are unable to process your request !");
                    alertDialogBuilder.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            } catch (Exception e) {
                dialog.dismiss();
                e.printStackTrace();
            }
        }
    }

}
