package demo.pickemup;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import demo.pickemup.adapter.DensityViewHolder;
import demo.pickemup.adapter.ProviderModel;
import demo.pickemup.adapter.UserRequestViewHolder;
import demo.pickemup.helper.Cache;
import demo.pickemup.helper.ServiceHelper;
import demo.pickemup.helper.Url;
import demo.pickemup.vo.UserRequestsVO;

public class RequestListActivity extends BaseAppActivity {


    private String userid;
    private String routeid;
    private String startpoint;
    private String endpoint;
    private String starttimehr;
    private String starttimemin;
    ListView requestList;
    private Button acceptBtn;
    private ArrayList<String> requestids=new ArrayList<>();
    private String requestidsStr="";
    ProgressDialog pd;

    HashMap<String,String> orginMaster=new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_request_list);
        configureLayout();

        acceptBtn = (Button) findViewById(R.id.acceptBtn);

        userid = sharedPref.getString("userid", "0");

        Bundle bundle=getIntent().getExtras();
        routeid=bundle.getString("routeid");
        starttimehr=bundle.getString("starttimehr");
        starttimemin=bundle.getString("starttimemin");
        startpoint=bundle.getString("startpoint");
        endpoint=bundle.getString("endpoint");
        requestList=(ListView)findViewById(R.id.requestList);
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        new PopulateRequestsTask().execute(Url.USERSFORROUTEANDSLOT+"?route-id="+routeid+"&start-date="+dateFormat.format(date)+"&start-time="+starttimehr+":00:00");

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<requestList.getAdapter().getCount();i++){
                    UserRequestViewHolder userRequestViewHolder=(UserRequestViewHolder) requestList.getAdapter();
                    if(userRequestViewHolder.isChecked(i)){
                        requestids.add(userRequestViewHolder.getItem(i).getRqeuestid());
                        requestidsStr+=userRequestViewHolder.getItem(i).getRqeuestid()+",";
                    }
                }
                if(requestids.size()!=0){
                    new SaveTripTask().execute(Url.SAVETRIP);
                }else{
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(RequestListActivity.this);
                    alertDialogBuilder.setMessage("Please select atleast one passenger!");
                    alertDialogBuilder.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            }
        });
    }

    private class PopulateRequestsTask extends AsyncTask<String, String, String> {
        private String responseData;
        private final ProgressDialog dialog = new ProgressDialog(RequestListActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage("Processing...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            responseData=new ServiceHelper().callGetService(urlString);
            return responseData;
        }

        protected void onPostExecute(String result) {
            ArrayList<UserRequestsVO> userRequestsVOS=new ArrayList<>();
            dialog.dismiss();
            if(responseData.contains("errCode")){
                AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(RequestListActivity.this);
                alertDialogBuilder.setTitle("Failed");
                alertDialogBuilder.setMessage("We are unable to fetch passenger list !");
                alertDialogBuilder.setPositiveButton(
                        "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }else{
                try{
                    JSONArray jsonArray = new JSONArray(responseData);
                    if(jsonArray.length()==0){
                        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(RequestListActivity.this);
                        alertDialogBuilder.setTitle("Failed");
                        alertDialogBuilder.setMessage("No user request found for the selected time period !");
                        alertDialogBuilder.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        Intent intent = new Intent(getApplicationContext(), ProviderActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        getApplicationContext().startActivity(intent);
                                        RequestListActivity.this.startActivity(intent);
                                        RequestListActivity.this.finish();
                                    }
                                });
                        final AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }else{
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonUserRequest=(JSONObject) jsonArray.get(i);
                            UserRequestsVO userRequestsVO=new UserRequestsVO();
                            userRequestsVO.setEndpointid(jsonUserRequest.getJSONObject("tripRequest").getString("endPointId"));
                            userRequestsVO.setImageurl(jsonUserRequest.getJSONObject("requestedBy").getString("imageURL"));
                            userRequestsVO.setStartpointid(jsonUserRequest.getJSONObject("tripRequest").getString("startPointId"));
                            userRequestsVO.setOrigin(Cache.getLocationMapCache(sharedPref).get(Integer.parseInt(userRequestsVO.getStartpointid())));
                            userRequestsVO.setDestination(Cache.getLocationMapCache(sharedPref).get(Integer.parseInt(userRequestsVO.getEndpointid())));
                            userRequestsVO.setTimestart(jsonUserRequest.getJSONObject("tripRequest").getString("journeyTime"));
                            userRequestsVO.setRqeuestid(jsonUserRequest.getJSONObject("tripRequest").getString("id"));
                            userRequestsVO.setUserid(jsonUserRequest.getJSONObject("tripRequest").getString("userId"));
                            userRequestsVO.setUsername(jsonUserRequest.getJSONObject("requestedBy").getString("name"));
                            userRequestsVOS.add(userRequestsVO);
                        }
                        UserRequestViewHolder densityViewHolder=new UserRequestViewHolder(userRequestsVOS,RequestListActivity.this);
                        requestList.setAdapter(densityViewHolder);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    private class SaveTripTask extends AsyncTask<String, String, String> {
        private String responseData;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(RequestListActivity.this);
            pd.setMessage("Loading…");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            InputStream in = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(urlString);
                JSONObject jsonObject=new JSONObject();
                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                jsonObject.put("routeId",routeid);
                jsonObject.put("providedBy",userid);
                jsonObject.put("startDate",dateFormat.format(date));
                jsonObject.put("startTime",starttimehr+":"+starttimemin);
                jsonObject.put("startPoint",startpoint);
                jsonObject.put("endPoint",endpoint);
                jsonObject.put("requestPoolId",requestids);

                String json="{\"routeId\":"+routeid+",\"providedBy\":"+userid+",\"startDate\":\""+dateFormat.format(date)+"\",\"startTime\":\""+starttimehr+":"+starttimemin+
                        "\",\"startPoint\":"+startpoint+",\"endPoint\":"+endpoint+",\"requestPoolId\":["+requestidsStr.substring(0,requestidsStr.length()-1)+"]}";

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                System.out.println(jsonObject.toString());

                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                //writer.write(jsonObject.toString());
                writer.write(json);
                writer.flush();
                writer.close();
                // Get the server response
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                responseData = sb.toString();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseData;
        }

        protected void onPostExecute(String result) {
            try {
                pd.dismiss();
                if (responseData != null && !responseData.contains("errCode")) {
                    Toast.makeText(getApplicationContext(), "Trip saved successfully",
                            Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("saveTripFlag", "Y");
                    editor.putString("saveTripId", responseData);
                    editor.commit();
                    getApplicationContext().startActivity(intent);
                } else {
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(RequestListActivity.this);
                    alertDialogBuilder.setTitle("Failed");
                    alertDialogBuilder.setMessage("Unable to save this trip !");
                    alertDialogBuilder.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
